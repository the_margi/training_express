const { Sequelize } = require('sequelize');

const database = new Sequelize('training_express', 'root', '', {
    host: 'localhost',
    dialect: 'mysql',
    define: {
        freezeTableName: true,
        timestamps: true,
    }
});


module.exports = database;