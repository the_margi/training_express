const { DataTypes } = require('sequelize');

module.exports = (database) => {
    const model = database.define('zoo', {
        name: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        location: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        establishmentDate: {
            type: DataTypes.DATEONLY,
            allowNull: false,
        },
    });

    return model;
};
