const database = require('../config/db');

const CrocoModel = require('./croco');
const ZooModel = require('./zoo');

const CrocoDb = CrocoModel(database);
const ZooDb = ZooModel(database);

ZooDb.hasMany(CrocoDb);
CrocoDb.belongsTo(ZooDb);

module.exports = {
    CrocoDb,
    ZooDb,
}